# Online Casino
This is a sample online casino built using React, Redux, React Router v4 and Semantic UI Components for usecase of loading in Netent/Betsson/A8rloader (as is made by such).

Netent game parse/launch script for games launched, afterwards callback forms are submitted from for example:
https://git.wainwright.store/jampack/example_jampack, and responses saved in cookie then in other page is a event listener.

https://comeon-game-test.casinomodule.com/admin/
https://nggcom-game-test.casinomodule.com/admin

nyxx group going ham i guess, please be noted that this cannot be a mistake, like the "mistake" they will blame on wordpress as described below:

The difference is the active changes required to even make this mistake is something David && Softswiss's Ilya (cto and real owner)  would not do, as someone who's been around for over 40 yrs in casino industry.


## Footnote
**Please note all the crap wordpress in many software providers popping up concurrently. 

As in preperation to blame something, they will be blaming faulty wordpress plugins for this fact, however the real reason is just easy way to submit forms in 3rd party using existing cookies of players in their browser, like facebook/twitter etc., or they are loaded in by their 'given up' providers to be used at a later point more covert.**

Inb4 certificate issues.**

Also be noted for example Ilya switching, Dejan boys setting up new gameart/3pi slotproviders 3oaks.net etc etc - it's silence before storm I suppose.

## Mock
```json
/* use module pattern to invoke the IIFE and return launch function which accepts a game parameter */
const comeon = (() => {
    const games = {
        starburst: {
            src: "https://comeon-static-test.casinomodule.com/games/starburst_mobile_html/game/starburst_mobile_html.xhtml?server=https%3A%2F%2Fcomeon-game-test.casinomodule.com%2F&lang=sv&sessId=DEMO-41e133d5237c402-EUR&gameId=starburst_not_mobile_sw&operatorId=default&staticsharedurl=http%3A%2F%2Fstatic-shared.casinomodule.com%2Fgameclient_html%2Fdevicedetection%2Fcurrent"
        }
        ,
        jackhammer: {
            src: "https://comeon-static-test.casinomodule.com/games/jackhammer_mobile_html/game/jackhammer_mobile_html.xhtml?server=https%3A%2F%2Fcomeon-game-test.casinomodule.com%2F&lang=sv&sessId=DEMO-0b3a6e21685c42a-EUR&gameId=jackhammer_not_mobile_sw&operatorId=default&staticsharedurl=http%3A%2F%2Fstatic-shared.casinomodule.com%2Fgameclient_html%2Fdevicedetection%2Fcurrent"
        }
        ,
        jackandbeanstalk: {
            src: "https://comeon-static-test.casinomodule.com/games/jackandbeanstalk_mobile_html/game/jackandbeanstalk_mobile_html.xhtml?server=https%3A%2F%2Fcomeon-game.casinomodule.com%2F&lang=en&sessId=DEMO-756f72b48bc2493-EUR&gameId=jackandbeanstalk_not_mobile_sw&operatorId=default&staticsharedurl=http%3A%2F%2Fstatic-shared.casinomodule.com%2Fgameclient_html%2Fdevicedetection%2Fcurrent"
        }
        ,
        deadoralive: {
            src: "https://comeon-static-test.casinomodule.com/games/deadoralive_mobile_html/game/deadoralive_mobile_html.xhtml?server=https%3A%2F%2Fcomeon-game-test.casinomodule.com%2F&lang=sv&sessId=DEMO-979bbc939ea9412-EUR&gameId=deadoralive_not_mobile_sw&operatorId=default&staticsharedurl=http%3A%2F%2Fstatic-shared.casinomodule.com%2Fgameclient_html%2Fdevicedetection%2Fcurrent"
        }
        ,
        twinspin: {
            src: "https://comeon-static-test.casinomodule.com/games/twinspin_mobile_html/game/twinspin_mobile_html.xhtml?server=https%3A%2F%2Fcomeon-game.casinomodule.com%2F&lang=en&sessId=DEMO-c813546a446a412-EUR&gameId=twinspin_not_mobile_sw&operatorId=default&staticsharedurl=http%3A%2F%2Fstatic-shared.casinomodule.com%2Fgameclient_html%2Fdevicedetection%2Fcurrent"
        }
    }
    ;
    return {
        game: {
            launch:function(game) {
            	if(!games[game]){
            		return;
            	}
            	const $gameLauncher = document.getElementById('game-launch');
                const $gameIframe = `<iframe 
                						src="${games[game].src}" 
                						id="game" 
                						frameborder="0" 
                						scrolling="no"
                						width="640"
                						height="480"
                					></iframe>`;
                $gameLauncher.innerHTML = $gameIframe;
                return true;
            }
        }
    }
})();

export default comeon;
```

### Install server dependencies
Install json-server needed to run the mock server
```javascript
npm install -g json-server
```

### Install UI dependencies
```javascript
npm install
or
yarn install
```

### Usage
This command runs both the server and app
```javascript
npm start
```

### Testing
To run unit tests
```javascript
yarn test
```

### For production build
```javascript
npm run build
```

